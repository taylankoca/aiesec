<?php

namespace App\Http\Controllers;

use App\User;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->is_payment == 0) {
            return redirect()->route('pay');
        } else {
            return redirect()->route('video');
        }
    }

    public function pay()
    {
        if (auth()->user()->is_payment == 0) {
            return view('pay');
        }
        return redirect()->route('video');
    }

    public function paymentoperation()
    {

        $posnet = new \Posnet();
        $posnet->SetUsername('iktisadiveti');
        $posnet->SetPassword('Pos12345');

        //Gerçek yayında test modundan çıkar
        $pos = new \SanalPos\YapiKredi\Pos($posnet, '6706598320', '67013703', 'test');

        $data['global'] = trim(Input::get("global"));
        $data['cc-number'] = implode("", explode(" ", Input::get("cc-number")));
        $data['cc-exp'] = implode("", array_map('trim', explode("/", Input::get("cc-exp"))));
        $data['cc-exp'] = substr($data['cc-exp'], 0, 2).substr($data['cc-exp'], -2);
        $data['cc-cvc'] = trim(Input::get("cc-cvc"));

        $pos->krediKartiAyarlari($data['cc-number'], $data['cc-exp'], $data['cc-cvc']);
        //$pos->krediKartiAyarlari('4543600290478695', '1021', '000');

        $payment = false;

        if($data['global'] == "globalvolunteer") {
            $pos->siparisAyarlari(550.00, '0000SIPARISID0' . time(), 0);
            $payment = true;
        } else if($data['global'] == "globaltalent") {
            $pos->siparisAyarlari(1050.00, '0000SIPARISID0' . time(), 0);
            $payment = true;
        } else if($data['global'] == "globalentrepreneur") {
            $pos->siparisAyarlari(750.00, '0000SIPARISID0' . time(), 0);
            $payment = true;
        }

        if($payment) {
            $pos->dogrula();
            $sonuc = $pos->odeme();
            $sonuc_arr = simplexml_load_string($sonuc->posnet->strResponseXMLData);
            if(intval($sonuc_arr->approved)) {
                auth()->user()->is_payment = 1;
                auth()->user()->payment_type = $data['global'];
                auth()->user()->save();
                $data['success'] = true;



            } else {
                $data['success'] = false;
            }
            //print_r($data);
            return view('paymentoperation', $data);
        } else {
            $data['success'] = false;
            //print_r($data);
            return view('paymentoperation', $data);
        }


    }



    public function video()
    {
        if (auth()->user()->is_payment == 1) {
            return view('video');
        }
        return redirect()->route('pay');
    }
    public function userlist()
    {
        if (auth()->user()->user_type_id == 1) {
            $users = User::all();
            return view('userlist', ['users' => $users]);
        }
        return redirect()->route('pay');
    }

    public function inviteUserGet()
    {
        return view('invite-user');
    }

    public function inviteUserPost(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);

        if ($request->has('email') and $request->has('password')) {

            try {
                $user = new User();
                $user->name = $request->get('email');
                $user->email = $request->get('email');
                $user->password = Hash::make($request->get('password'));
                $user->user_type_id = 2;
                $user->is_payment = 0;
                $user->save();

                Mail::send('mail.invite', ['password' => $request->get('password'), 'message' => $request->get('message')], function ($m) {
                    $m->from('test@isbulun.com', 'Your Application');

                    $m->to('suleyman.yilmaz@beyn.com.tr', 'dsfdssdfsdf')->subject('Your Reminder!');
                });

            } catch (\Exception $e) {
                $e->getMessage();
            }

            return redirect()->back();
        }
    }
}
