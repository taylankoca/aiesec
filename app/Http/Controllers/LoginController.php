<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

        $userData = array(
            'email'     => $request->get('email'),
            'password'  => $request->get('password')
        );

        if (Auth::attempt($userData)) {
            return redirect()->route('dashboard');
        }else{
            return redirect('/')->with('status', 'Wrong Password!');
        }
    }
}
