<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('/pay', ['as' => 'pay', 'uses' => 'DashboardController@pay']);
    Route::post('/payment-operation', ['uses' => 'DashboardController@paymentoperation']);
    Route::get('/userlist', ['as' => 'userList', 'uses' => 'DashboardController@userlist']);
    Route::get('/video', ['as' => 'video', 'uses' => 'DashboardController@video']);
    Route::get('/invite-user', ['as' => 'inviteUser', 'uses' => 'DashboardController@inviteUserGet']);
    Route::post('/invite-user', ['as' => 'inviteUserPost', 'uses' => 'DashboardController@inviteUserPost']);

});

Route::get('/test', function () {

    //echo Hash::make('taylan99');
    //echo Request::getClientIp();



});

//Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'LoginController@index');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

//// Registration routes...
//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');