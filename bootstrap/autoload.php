<?php

define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';

require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/BasePos.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/PosInterface.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/PosSonucInterface.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/TaksitHesap.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/YapiKredi/Pos.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/YapiKredi/Sonuc.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/Est/Pos.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/Est/Sonuc.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/Garanti/Pos.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/src/SanalPos/Garanti/Sonuc.php';
require __DIR__.'/../vendor/ekrembk/sanal-pos/vendor/posnet/Posnet XML/posnet.php';

/*
|--------------------------------------------------------------------------
| Include The Compiled Class File
|--------------------------------------------------------------------------
|
| To dramatically increase your application's performance, you may use a
| compiled class file which contains all of the classes commonly used
| by a request. The Artisan "optimize" is used to create this file.
|
*/

$compiledPath = __DIR__.'/cache/compiled.php';

if (file_exists($compiledPath)) {
    require $compiledPath;
}
