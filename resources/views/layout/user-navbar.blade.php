<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">AIESEC</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if(auth()->user()->is_payment == 0)
                    <li>
                        <a href="{{ route('pay') }}" class="smoothScroll">Pay Now</a>
                    </li>
                @endif
                @if(auth()->user()->is_payment == 1)
                    <li>
                        <a href="{{ route('video') }}">Videos</a>
                    </li>
                @endif
                @if(auth()->user()->user_type_id == 1)
                        <li>
                            <a href="{{ route('inviteUser') }}">Invite User</a>
                        </li>
                        <li>
                            <a href="{{ route('userList') }}">User List</a>
                        </li>
                @endif
                <li>
                    <a href="#contact" class="smoothScroll">Contact</a>
                </li>
                <li>
                    <a href="/auth/logout" class="smoothScroll">Logout</a>
                </li>
            </ul>
        </div>

    </div>
</div>