
<!DOCTYPE html>
<html lang="en">
<head>
    <title>AIESEC - Pay</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- stylesheets css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <link rel="stylesheet" href="css/nivo-lightbox.css">
    <link rel="stylesheet" href="css/nivo_themes/default/default.css">

    <link rel="stylesheet" href="css/hover-min.css">
    <link rel="stylesheet" href="css/flexslider.css">

    <link rel="stylesheet" href="css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600' rel='stylesheet' type='text/css'>

</head>
<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- Preloader section -->
<div class="preloader">
    <div class="sk-spinner sk-spinner-pulse"></div>
</div>

@yield('content')

<!-- Footer section -->
<footer>
    <div class="container">
        <div class="row">

            <div class="wow fadeInUp col-md-6 col-sm-6 col-xs-12" data-wow-delay="0.3s">
                <h3>About the AIESEC</h3>
                <p>Welcome to AIESEC Turkey!</p>
            </div>

            <div class="wow fadeInUp col-md-6 col-sm-6 col-xs-12" data-wow-delay="0.6s">
                <h3>Contact Detail</h3>
                <h4>AIESEC Turkey</h4>
                <p>Tomtom Mah, Postacılar Sok. Şaup Apt 1:1 </p>
                <p>Taksim İstanbul</p>
                <p>info@aiesecturkey.com</p>
            </div>
        </div>
    </div>
</footer>

<!-- Copyright section -->
<section id="copyright">
    <div class="container">
        <div class="row">

            <div class="col-md-8 col-sm-8 col-xs-8">
                <p>Copyright © 2016 AIESEC - aiesecturkeylead.com
                </p>
            </div>

            <div class="col-md-4 col-sm-4 text-right">
                <a href="#home" class="fa fa-angle-up smoothScroll gototop"></a>
            </div>

        </div>
    </div>
</section>

@yield('js')

</body>
</html>
