@extends('layout.default')
@section('content')
    @include('layout.user-navbar')

    <!-- Feature section -->
    <section id="feature" class="parallax-section">
        <div class="container">
            <div class="row">

                <div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">
                    <div class="wow fadeInUp section-title" data-wow-delay="0.6s">
                        <h2>Payment Operation</h2>
                        <h4>Going On Now</h4>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                <div style="text-align: left; margin-left: 16px; font-size: 16px; margin-bottom: 20px; font-weight: bold;">
                    <p>Sonuç:</p>
                    @if ($success)
                        <p>İşlem başarı ile gerçekleştirildi.</p>
                    @else
                        <p>İşlem başarısız.</p>
                    @endif
                </div>
                </div>

            </div>
        </div>
    </section>

    @include('layout.contact')


@endsection

@section('js')
    <!-- javscript js -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/jquery.magnific-popup.min.js"></script>

    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.backstretch.min.js"></script>

    <script src="js/isotope.js"></script>
    <script src="js/imagesloaded.min.js"></script>
    <script src="js/nivo-lightbox.min.js"></script>

    <script src="js/jquery.flexslider-min.js"></script>

    <script src="js/jquery.parallax.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/wow.min.js"></script>

    <script src="js/jquery.payment.min.js"></script>

    <script src="js/custom.js"></script>
@endsection
