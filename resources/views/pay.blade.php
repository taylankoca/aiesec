@extends('layout.default')
@section('content')
    @include('layout.user-navbar')

    <!-- Feature section -->
    <section id="feature" class="parallax-section">
        <div class="container">
            <div class="row">

                <div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">
                    <div class="wow fadeInUp section-title" data-wow-delay="0.6s">
                        <h2>Pay Now</h2>
                        <h4>Choose the program</h4>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12">
                <div style="text-align: left; margin-left: 16px; font-size: 16px; margin-bottom: 20px; font-weight: bold;">
                    <form id="payment" name="payment" method="post" action="payment-operation" >

                    {{ csrf_field() }}

                    <p>Choose the program you are part of and pay the fee quickly</p>
                    <div class="radio" style="display: inline-block;margin-right: 10px;">
                        <label>
                            <input type="radio" name="global" id="input" value="globalvolunteer" checked="checked">
                            Global Volunteer - 550 TL
                        </label>
                    </div>
                    <div class="radio" style="display: inline-block;margin-right: 10px;">
                        <label for="input2">
                            <input type="radio" name="global" id="input2" value="globaltalent">
                            Global Talent - 1050 TL
                        </label>
                    </div>
                    <div class="radio" style="display: inline-block;margin-right: 10px;">
                        <label for="input3">
                            <input type="radio" name="global" id="input3" value="globalentrepreneur">
                            Global Entrepreneur - 750 TL
                        </label>
                    </div>
                </div>

                    <div id="payment-stripe" class="container">
                        <div class="row text-left">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="cc-number" class="control-label">Credit Card Number
                                        <small class="text-muted"><span data-payment="cc-brand"></span></small>
                                    </label>
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-credit-card"></i>
                                            </span>
                                        <input id="cc-number" name="cc-number" type="text" class="input-lg form-control cc-number"
                                               autocomplete="cc-number" placeholder="5555 5555 5555 5555"
                                               data-payment='cc-number' required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Expiration (MM/YYYY)</label>
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        <input id="cc-exp" name="cc-exp" type="text" class="input-lg form-control cc-exp"
                                               autocomplete="cc-exp" placeholder="•• / ••••" data-payment='cc-exp'
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>CVC Code</label>
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-lock"></i>
                                            </span>
                                        <input id="cc-cvc" name="cc-cvc" type="tel" class="input-lg form-control cc-cvc"
                                               autocomplete="off" placeholder="•••" data-payment='cc-cvc' required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit" id="validate">Pay It</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

    @include('layout.contact')


@endsection

@section('js')
    <!-- javscript js -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/jquery.magnific-popup.min.js"></script>

    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.backstretch.min.js"></script>

    <script src="js/isotope.js"></script>
    <script src="js/imagesloaded.min.js"></script>
    <script src="js/nivo-lightbox.min.js"></script>

    <script src="js/jquery.flexslider-min.js"></script>

    <script src="js/jquery.parallax.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/wow.min.js"></script>

    <script src="js/jquery.payment.min.js"></script>

    <script src="js/custom.js"></script>
@endsection
