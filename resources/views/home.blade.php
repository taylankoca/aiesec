@extends('layout.default')
@section('content')

    <!-- Home section -->
    <section id="home" class="parallax-section">
        <div class="gradient-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-sm-12">
                    <h1 class="wow fadeInUp" data-wow-delay="0.6s" style="font-size: 32px;">AIESEC in Turkey Online Education System</h1>
                    <p class="wow fadeInUp" data-wow-delay="1.0s">Welcome to your ''Once in a lifetime experience''  </p>
                    <button data-toggle="modal" data-target="#login" class="wow fadeInUp btn btn-default"
                            data-wow-delay="1.3s">Login
                    </button>
                </div>
            </div>
        </div>
    </section>

    @include('layout.navbar')

    <!-- Feature section -->
   <!--  <section id="feature" class="parallax-section">
        <div class="container">
            <div class="row">

                <div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">
                    <div class="wow fadeInUp section-title" data-wow-delay="0.6s">
                        <h2>Why Choose Us?</h2>
                        <h4>Lorem ipsum dolo sit amet..</h4>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="feature-thumb">
                        <div class="feature-icon">
                                <span>
                                    <i class="fa fa-cutlery"></i>
                                </span>
                        </div>
                        <h3>SPECIAL DISH</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet
                            phasellus.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="feature-thumb">
                        <div class="feature-icon">
                                <span>
                                    <i class="fa fa-coffee"></i>
                                </span>
                        </div>
                        <h3>BLACK COFFEE</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet
                            phasellus.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.9s">
                    <div class="feature-thumb">
                        <div class="feature-icon">
                                <span>
                                    <i class="fa fa-bell-o"></i>
                                </span>
                        </div>
                        <h3>DINNER</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elitquisque tempus ac eget diam et laoreet
                            phasellus.</p>
                    </div>
                </div>

            </div>
        </div>
    </section> -->

    <!-- About section -->
<!--     <section id="about" class="parallax-section">
        <div class="container">
            <div class="row">

                <div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">
                    <div class="wow fadeInUp section-title" data-wow-delay="0.3s">
                        <h2>Our Story</h2>
                        <h4>Lorem ipsum dolo sit amet..</h4>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="wow fadeInUp col-md-3 col-sm-5" data-wow-delay="0.3s">
                    <img src="images/home-bg-slideshow1.jpg" class="img-responsive" alt="About">
                    <h3>Nunc ullamcorper suscipit neque, ac malesuada purus molestie non.</h3>
                </div>

                <div class="wow fadeInUp col-md-5 col-sm-7" data-wow-delay="0.5s">

                    
                    <div class="flexslider">
                        <ul class="slides">

                            <li>
                                <img src="images/home-bg-slideshow2.jpg" alt="Flexslider">
                            </li>
                            <li>
                                <img src="images/home-bg-slideshow3.jpg" alt="Flexslider">
                            </li>
                            <li>
                                <img src="images/home-bg-slideshow4.jpg" alt="Flexslider">
                            </li>

                        </ul>
                    </div>

                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                        tincidunt. Lorem ipsum dolor sit amet.</p>
                </div>

                <div class="wow fadeInUp col-md-4 col-sm-12" data-wow-delay="0.9s">
                    <h2>Fine Dining</h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                        <a rel="nofollow" href="#" target="_blank">Tooplate</a>
                        site.</p>
                    <p>Vestibulum id iaculis nisl. Pellentesque nec tortor sagittis, scelerisque ante at, sollicitudin
                        leo. Vivamus pulvinar a justo vel lobortis.</p>

                    <ul>
                        <li>Donec fringilla ipsum</li>
                        <li>Integer nec urna</li>
                        <li>Curabitur porta</li>
                    </ul>
                </div>

            </div>
        </div>
    </section> -->

    <!-- Contact section -->
    <section id="contact" class="parallax-section">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">

                <div class="col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">
                    <div class="wow fadeInUp section-title" data-wow-delay="0.3s">
                        <h2>Say hello</h2>
                        <h4>we are always ready to serve you!</h4>
                    </div>
                    <div class="contact-form wow fadeInUp" data-wow-delay="0.7s">
                        <form id="contact-form" method="post" action="#">
                            <input name="name" type="text" class="form-control" placeholder="Your Name" required>
                            <input name="email" type="email" class="form-control" placeholder="Your Email" required>
                            <textarea name="message" class="form-control" placeholder="Message" rows="5"
                                      required></textarea>
                            <input type="submit" class="form-control submit" value="SEND MESSAGE">
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>



    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="loginLabel">Login</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/auth/login">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input name="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input name="password" type="password" class="form-control" id="exampleInputPassword1"
                                   placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

    @if (session('status'))
        <script>
            alert("{{ session('status') }}");
        </script>
    @endif

    <!-- javscript js -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/jquery.magnific-popup.min.js"></script>

    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.backstretch.min.js"></script>

    <script src="js/isotope.js"></script>
    <script src="js/imagesloaded.min.js"></script>
    <script src="js/nivo-lightbox.min.js"></script>

    <script src="js/jquery.flexslider-min.js"></script>

    <script src="js/jquery.parallax.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/wow.min.js"></script>

    <script src="js/custom.js"></script>
@endsection
